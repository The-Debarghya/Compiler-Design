%{
/* Definition section */
    #include<stdio.h>
    int yylex(void);
    void yyerror(char *);
    int flag=0;
    extern FILE *yyin;
	extern FILE *yyout;
%}

%token NUMBER 

%left '+' '-'

%left '*' '/'

%left '&' '|'

%left '(' ')'

/* Rule Section */
%%

ArithmeticExpression: E { printf("\nResult=%d\n", $$); return 0; } ;

E: E'+'E {$$=$1+$3;} | E'-'E {$$=$1-$3;} | E'*'E {$$=$1*$3;} | E '/'E {$$=$1/$3;} | E'|'E {$$=$1|$3;} | E'&'E {$$=$1&$3;} | '('E')' {$$=$2;} | NUMBER {$$=$1;} ;

%%

//driver code
int main() {
    printf("\nEnter expression:\n");

    yyparse();
    if(flag==0){
        printf("\nEntered arithmetic expression is Valid\n\n");
    }
}

void yyerror(char *s) {
    fprintf(stderr, "%s\n", s);
    flag=1;
}