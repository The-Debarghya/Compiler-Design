%{
    #include <stdio.h>
    #include <string.h>
    #include "parser.tab.h"
    void yyerror(char *);
%}

binary    [01]+b
octal     [0-7]+o

%%

{binary}  { yytext[yyleng-1] = '\0'; yylval = strtoll(yytext, NULL, 2); printf("%d ", yylval); return NUMBER; }
{octal}  { yytext[yyleng-1] = '\0'; yylval = strtoll(yytext, NULL, 8); printf("%d ", yylval); return NUMBER; }
[-+*/&|)(\n]  return *yytext;
[ \t]   ; /* skip whitespace */
.       {printf("invalid character %s", yytext); yyerror("invalid character");}

%%

int yywrap() {
    return 1;
}