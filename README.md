# Compiler-Design
- Assignments given as a part of Compiler Design Labs in BCSE course.
- `Assn1`: Basic lex/flex examples.
- `Assn2`: Lexical error analysis of C-like language.
- `Assn3`: Some bison/yacc examples.
- `Assn4`: Left recursion removal, both direct and indirect.
- `Assn5`: Calculator application that computes only binary and octal numbers.
- `Assn_3`: Lexical and syntax analysis of C like language.
