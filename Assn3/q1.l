%{
    # include <stdio.h>
%}

%%
A+B+ { printf("Correct!"); }
.+ { printf("Incorrect!"); }
%%
int yywrap(void) {
    return 1;
}
int main(void) {
    yylex();
    return 0;
}