%{
    #include<stdio.h>
    #include<stdlib.h>
    int yylex(void);
    void yyerror(char *);
%}

%token A B NL

%%
stmt: S NL { printf("valid string\n"); exit(0); } ;
S: A S B | ;
%%

void yyerror(char *msg) {
    printf("invalid string\n");
    exit(0);
}

//driver code
int main() {
    printf("enter the string\n");
    yyparse();
}
