%{
    int op = 0,i;
    float a, b;
%}

dig [0-9]+|([0-9]*)"."([0-9]+)
add "+"
sub "-"
mul "*"
div "/"
pow "^"
mod "%"
ln \n

%{
void calculate() {
    if(op==0)
        a=atof(yytext);
    else {
        b=atof(yytext);
        switch(op) {
            case 1:
                a=a+b;
                break;

            case 2:
                a=a-b;
                break;

            case 3:
                a=a*b;
                break;

            case 4:
                a=a/b;
                break;

            case 5:
                for(i=a;b>1;b--)
                    a=a*i;
                break;
            case 6:
                a=(int)a%(int)b;
                break;
        }
        op=0;
    }
}
%}

%%

{dig} { calculate(); }
{add} { op = 1; }
{sub} { op = 2; }
{mul} { op = 3; }
{div} { op = 4; }
{pow} { op = 5; }
{mod} { op = 6; }
{ln} { printf("Result: %f\n", a); }

%%

int yywrap(void) {
    return 1;
}

int main(int argv,char *argc[]) {
    yylex();
    return 0;
}
